# Python program to interchange first and last elements in a list
# ----------------------------------------------------------2022-03-19--------------------------------------------------
# def interchange(new):
#     # solution 1
# 2022-03-19
# def interchange(new):
    # solution 1
#     size = len(a)
#     print(size)
#
#     temp = a[0]
#     print(temp)
#     a[0] = a[size - 1]
#     print(a[0])
#     a[size - 1] = temp
#     print(a[size - 1])
#
#     return a
#     #  solution 2
#     a[0],a[-1] = a[-1],a[0]
#     return a
#     # solution 3
#     a,*b,c = new
#     new = [c,*b,a]
#     print(new)
#
#     return new
#
#     # solution 4
#     first = new.pop(0)
#     last=new.pop(-1)
#
#     new.append(first)
#     # new.insert(0,last)
#     # new.append(first)
#     new.insert(0,last)
#     return new
#
# a = ['a','b','c','d','e','f']
# print(interchange(a))
#

# ************************************Python program to swap two elements in a list*************************************

# # Python program to swap two elements in a list

# def swap_any(list):
#     # solution 1
#     list[0], list[2] = list[2], list[0]
#     return list
#
# list = ['a','j','m','a','l']
# print(swap_any(list))


# As same as above one but - passing the two arguments in the function and assigning the values and then passing to the list and return.

# -------------------------------------------------------------2022-03-21-----------------------------------------------
# ***********************************************Python – Swap elements in String list**********************************
# solution 1
# test_list = ['ajmal', 'kaaru', 'aasu', 'karthi']
# str(test_list)
# new = [i.replace('a', '_') for i in test_list]
#     i.replace('a', '_')
#     i.replace('k','a')
#     i.replace('j','-')
# for i in test_list[::-1]:
#     new = [i.replace('a','*').replace('k','$').replace('*','^')]
#     print(new)

# solution 2
# new = '@'.join(test_list)
# print(new)
# new = new.replace('a','*').replace('k','$').replace('*','^').split('@')
# print(new)
# new1 = test_list[2:-1]
# new = new1.replace('a','*').replace('k','$').replace('*','^').split('@')
# print(new)

#*************************************************** Python | Ways to find length of list*******************************
# solution1
# test_list = ['ajmal', 'kaaru', 'aasu', 'karthi']
# new = len(test_list)
# print(new)
#
# # solution2
# from operator import length_hint
# new1 = length_hint(test_list)
# print(new1)

# **************************************************Maximum of two numbers in Python************************************
# solution1
# num = 5
# num1 = 10
# if num > num1:
#     print("greater")
# else:
#     print("smaller")

# solution2
# def maxi(num,num1):
#     if num > num1:
#         print("greater")
#     else:
#         print("smaller")
# num = 15
# num1 = 10
# maxi(num,num1)
# print(maxi(num,num1))

# solution3
# new = max(num,num1)
# print(new)

# *******************************************Minimum of two numbers in Python*******************************************
# As same as above one but - change the max to min.

# As same as above one but - passing the two arguments in the function and assigning the values and then passing to the list and return.


# -------------------------------------------------------------2022-03-21-----------------------------------------------
# ***********************************************Python – Swap elements in String list**********************************
# solution 1
# test_list = ['ajmal', 'kaaru', 'aasu', 'karthi']
# str(test_list)
# new = [i.replace('a', '_') for i in test_list]
#     i.replace('a', '_')
#     i.replace('k','a')
#     i.replace('j','-')
# for i in test_list[::-1]:
#     new = [i.replace('a','*').replace('k','$').replace('*','^')]
#     print(new)

# solution 2
# new = '@'.join(test_list)
# print(new)
# new = new.replace('a','*').replace('k','$').replace('*','^').split('@')
# print(new)
# new1 = test_list[2:-1]
# new = new1.replace('a','*').replace('k','$').replace('*','^').split('@')
# print(new)

#*************************************************** Python | Ways to find length of list*******************************
# solution1
# test_list = ['ajmal', 'kaaru', 'aasu', 'karthi']
# new = len(test_list)
# print(new)
#
# # solution2
# from operator import length_hint
# new1 = length_hint(test_list)
# print(new1)

# **************************************************Maximum of two numbers in Python************************************
# solution1
# num = 5
# num1 = 10
# if num > num1:
#     print("greater")
# else:
#     print("smaller")

# solution2
# def maxi(num,num1):
#     if num > num1:
#         print("greater")
#     else:
#         print("smaller")
# num = 15
# num1 = 10
# maxi(num,num1)
# print(maxi(num,num1))

# solution3
# new = max(num,num1)
# print(new)

# *******************************************Minimum of two numbers in Python*******************************************
# As same as above one but - change the max to min.

# ***************************************Check if element exists in list in Python**************************************
# solution1
# test_list = [13,4,5,66,2,44,6,9,0,4,5,13,2]
# if 10 in test_list:
#     print("in")
# else:
#     print("no")

# solution2
# new = test_list.count(4)
# print(new)

# solution3
# test_list1 = set(test_list)
# print(test_list1)
# new = test_list1.count(4)
# print(new)
# if 4 in test_list1:
#     print("in")
# else:
#     print("no")

# *************************Different ways to clear a list in Python*****************************************************
# keyword- clear,delete
# solution1
# test_list1 = [13,4,5,66,2,44,6,9,0,4,5,13,2]
# test_list2 = ['ajmal', 'kaaru', 'aasu', 'karthi']

# test_list1.clear()
# print(test_list1,test_list2)

# solution2
# del test_list1
# print(test_list1,test_list2)

# ******************************Reversing a List in Python**************************************************************
# solution1
# for i in test_list1[::-1]:
#     print(i)
#
# # solution2
# def rev(test_list1):
#     test_list1.reverse()
#     return test_list1
#
# print(rev(test_list1))

# *************************************Python | Cloning or Copying a list***********************************************
# keyword - cloning
# def cloning(test_list1):
#     copy = [i for i in test_list1]
#     return copy
#
# test_list1 = [13,4,5,66,2,44,6,9,0,4,5,13,2]
# test_list3 = cloning(test_list1)
# print(cloning(test_list1))

# ************************Python | Count occurrences of an element in a list********************************************
# keyword - count, counter
# solution1
# test_list1 = [13,4,5,66,2,44,6,9,0,4,5,13,2]
# for i in test_list1:
#     num = test_list1.count(i)
#     print("number:",i,"count: ",num)

# solution2
# def indi(test_list1,x):
# def indi(x):
#     # for i in test_list1:
#     test_list1 = [13, 4, 5, 66, 2, 44, 6, 9, 0, 4, 5, 13, 2]
#     num = test_list1.count(x)
#     print("number:", x, "count: ", num)

# test_list1 = [13, 4, 5, 66, 2, 44, 6, 9, 0, 4, 5, 13, 2]
# x = 4
# print(indi(x))
# print(indi(test_list1,x))

# solution3
# from collections import Counter
# test_list1 = [13,4,5,66,2,44,6,9,0,4,5,13,2]
# new = Counter(test_list1) #it gives value in dictionary
# print(new)

# **************************************Find sum and average of List in Python******************************************
# keyword - sum(), avg()

# ******************************Python | Sum of number digits in List***************************************************
# solution1
# test_list = [13, 66, 44, 6, 9, 13, 2]
# ori = []
# for i in test_list:
#     s = 0
#     for j in str(i):
#         s += int(j)
#         print(s)
#     ori.append(s)
# print(ori)

# solution2
# a = list(map(lambda j: sum(int(i) for i in str(j)), test_list))
# print(a)

# **********************************Python | Multiply all numbers in the list ******************************************
# solution1
# test_list = [13, 66, 44, 6, 9, 13, 2]
# ori = []
# for i in test_list:
#     s = 1
#     for j in str(i):
#         s *= int(j)
#         print(s)
#     ori.append(s)
# print(ori)

# ori = []
# s =1
# for i in test_list:
#     s *= int(i)
#     print(s)
# ori.append(s)
# print(ori)

# solution2
# from functools import reduce
# a = reduce((lambda x,y : x * y), test_list)
# print(a)
#
# import math
# test_list = [6, 9, 2]
# a = math.prod(test_list)
# print(a)

# ****************************************Python program to find smallest number in a list******************************
# solution1
# test_list = [13, 66, 44, 6, 9, 13, 2]
# new = [min(test_list)]
# print(new)

# solution2
# new = test_list.sort()
# min_value = *new[:1]  #Unable to store sort value in variable, this solution will not work
# print(new)
# print(*new[:1])

# test_list  = [13, 66, 44, 6, 9, 13, 2]
# print(test_list .sort())
# print(*test_list [:3])

# -----------------------------------------------------2022-03-22-------------------------------------------------------

# solution3
# li = []
# user = int(input("list number:" ))
# for i in range(1, user+1):
#     a = int(input())
#     li.append(a)
# print(min(li))

# **************************************Python program to find largest number in a list*********************************

# As same as above one but - change the min to max.

# *********************************Python program to find second largest number in a list*******************************
# solution1
# test_list = [13, 66, 44, 6, 9, 13, 2]
# new = test_list.remove(max(test_list))
# print(max(test_list))

# solution2
# test_list.sort()
# print(test_list[-2])

# ********************************Python program to print even numbers in a list****************************************
# solution1
# test_list = [13, 66, 44, 6, 9, 13, 2]
# for i in test_list:
#     if i % 2 == 0:
#         print("given number",i, "even")
#     else:
#         print("given number",i, "odd")

# ********************************Python program to print odd numbers in a list*****************************************

# As same as above one but - change the (i % 2 != 0)

# *********************************Python program to print all even numbers in a range**********************************
# solution1
# test_list = [13, 66, 44, 6, 9, 13, 2]
# for i in test_list:
#     if i % 2 == 0:
#         print(i)

# solution2
# range1 = 2
# range2 = 99
# for i in range(range1, range2+1):
#     if i % 2 == 0:
#         print(i)

# solution3
# range1 = int(input())
# range2 = int(input())
# for i in range(range1, range2+1):
#     if i % 2 == 0:
#         print(i)

# *********************************Python program to print all odd numbers in a range***********************************
# solution1
# test_list = [13, 66, 44, 6, 9, 13, 2]
# for i in test_list:
#     if i % 2 != 0:
#         print(i)

# solution2
# range1 = 2
# range2 = 99
# for i in range(range1, range2+1):
#     if i % 2 != 0:
#         print(i)

# solution3
# range1 = int(input())
# range2 = int(input())
# for i in range(range1, range2+1):
#     if i % 2 != 0:
#         print(i)

# *********************************Python program to count Even and Odd numbers in a List*******************************
# solution1
# test_list = [13, 66, 44, 6, 9, 13, 2]
# ecount = 0
# ocount = 0
# for i in test_list:
#     if i % 2 == 0:
#         ecount += 1
#     else:
#         ocount += 1
# print("odd count", ocount)
# print("even count", ecount)

# solution2
# even = len(list(filter(lambda x: x % 2 == 0, test_list)))
# odd = len(list(filter(lambda x:x % 2 != 0, test_list)))
# print(even,odd)

#**************************************************Python program to print positive numbers in a list*******************
# solution1
# test_list = [-13, 66, -44, 6, -9, 13, -2,0]
# for i in test_list:
#     if i < 0:
#         print("negative",i)
#     elif i == 0:
#         print("origin",i)
#     else:
#         print("positive", i)

# solution2
# positive = list(filter(lambda x: x > 0, test_list))  #result will be in list form
# negative = list(filter(lambda x: x < 0, test_list))  #result will be in list form
# print(positive,negative)

# *********************************************Python program to print negative numbers in a list**********************
# solution1
# test_list = [-13, 66, -44, 6, -9, 13, -2,0]
# for i in test_list:
#     if i < 0:
#         print("negative",i)
#     elif i == 0:
#         print("origin",i)
#     else:
#         print("positive", i)

# solution2
# positive = list(filter(lambda x: x > 0, test_list))  #result will be in list form
# negative = list(filter(lambda x: x < 0, test_list))  #result will be in list form
# print(positive,negative)

# ********************************Python program to print all positive numbers in a range*******************************
# solution1
# range1 = 2
# range2 = 10
# for i in range(range1, range2+1):
#     if i > 0:
#         print(i)

# solution2
# range1 = int(input())
# range2 = int(input())
# for i in range(range1, range2+1):
#     if i > 0:
#         print(i)

# ********************************Python program to print all negative numbers in a range*******************************
# solution1
# range1 = -20
# range2 = -10
# for i in range(range1, range2+1):
#     if i < 0:
#         print(i)

# solution2
# range1 = int(input())
# range2 = int(input())
# for i in range(range1, range2+1):
#     if i < 0:
#         print(i)

# *********************************Python program to count positive and negative numbers in a list**********************
# solution1
# test_list = [13, -66, -44, 6, -9, 13, 2]
# pcount = 0
# ncount = 0
# for i in test_list:
#     if i < 0:
#         pcount += 1
#     else:
#         ncount += 1
# print("odd count", pcount)
# print("even count", ncount)

# solution2
# positive = len(list(filter(lambda x: x > 0, test_list)))
# negative = len(list(filter(lambda x:x < 0, test_list)))
# print(positive,negative)

# **********************************Remove multiple elements from a list in Python**************************************
# solution1
# test_list = [9, 13, 66, -44, 6, 13, 2]
# del test_list[:3]
# print(test_list)

# solution2
# new = set(test_list)
# print(new)

# test_list = [3,6]
# for i in test_list:
#     if i % 3 == 0:
#         test_list.remove(i)
# print(test_list)

# **************************************Python | Remove empty tuples from a list****************************************

# test_list = ('h','i')
# print(type(test_list))
# test_list1 = filter(None, test_list)
# print(test_list1,test_list)

# solution1
# def remo(test_list):
#     a = [i for i in test_list if i]
#     return a
#
# test_list = [('p', 'r', 'o', 'g', 'r', 'a', 'm', 'i', 'z'), (), ('q', 'q')]
# print(remo(test_list))

# ******************************************Python | Program to print duplicates from a list of integers****************
# solution1
test_list = [1,2,3,4,4,5,6,6,6,3,2,2,142,4,3,8,5,6,9,2,4,353,344,5,5]
# for i in test_list:
#     new = test_list.count(i)
#     # print("number:",i, new)
#     if new > 1:
#         print("duplicate number count:",i, "-" , new)
# print(test_list)

# solution2
# dupli = []
# for i in test_list:
#     new = test_list.count(i)
#     if new > 1:
#         if dupli.count(i) == 0:
#             dupli.append(i)
# print(dupli)

dupli = []
for i in test_list:
    new = test_list.count(i)
    if new == 1:
        if dupli.count(i) == 0:
            dupli.append(i)
print(dupli)
print(len(dupli))