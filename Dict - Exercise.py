# ******************************Python | Sort Python Dictionaries by Key or Value***************************************
# test_dict = {
#         "dbname": "ajmal",
#         "tenant_dbname": "1234567890",
#         "host": "amazonaws.com",
#         "rr_host": "amazonaws.com",
#         "username": "sample",
#         "password": "password",
#         "echo": "True"
#       }
# -------------------------------------------2022-03-22-----------------------------------------------------------------
# soltion1
# test_dict= {5:20, 6: 10, 13: 50, 24: 40, 3: 30}
# test_dict1 = {}
# for i in sorted(test_dict.items()):
#     print(type(i))                            # it will sort but not return as dict, it will return as tuple

# solution2
# test_dict1 = {}
# for i in sorted(test_dict):
#     test_dict1[i] = test_dict[i]
# print(test_dict1)

# solution3
# test_dict1 = dict(sorted(test_dict.items() ,key = lambda  x : x[1]))
# print(test_dict1)

# **************************Handling missing keys in Python dictionaries************************************************
# keyword - get, setdefault
# solution1
# try:
# test_dict = {
#     "dbname": "ajmal",
#     "tenant_dbname": "1234567890",
#     "host": "amazonaws.com",
#     "rr_host": "amazonaws.com",
#     "username": "sample",
#     "password": "password",
#     "echo": "True"
#   }
#     print(test_dict[2])
# except:
#     print("key or value not available")

# solution2
# a = test_dict.get("host", "value kanavillai or kidaikavillai")
# b = test_dict.get("ajmal", "value kanavillai or kidaikavillai")
# a = test_dict.get("host")
# b = test_dict.get("echo")
# print(a,b)

# solution3
# a = test_dict.setdefault("ajmal", "No value or key")
# print(test_dict["ajmal"], test_dict["dbname"])

# *****************************Python dictionary with keys having multiple inputs***************************************
# ----------------------------------------------2022-03-23--------------------------------------------------------------
# test_dict1 = {
#         ("database","dbname"): "ajmal",
#         ("tenant","dbname"): "1234567890"}
# database_n = []
# db = []
# tenant = []
# for i in test_dict1:
#         database_n.append(i[0])
#         db.append(i[1])
#         tenant.append(test_dict1[i[0],i[1]])
# print(database_n,db,tenant)

# *********************************Python program to find the sum of all items in a dictionary**************************
# solution1
# test_dict= {5:20, 6: 10, 13: 50, 24: 40, 3: 30}
# add = 0
# for i in test_dict.values():
#     add = add + i
# print(add)

# solution2
# def sum_values(test_dict):
#     add = 0
#     for i in test_dict.values():
#         add = add + i
#     return add
# test_dict = {5:20, 6: 10, 13: 50, 24: 40, 3: 30}
# print(sum_values(test_dict))

# *********************************Find the size of a Dictionary in Python**********************************************
# keyword - __size__
# solution1
# test_dict= {5:20, 6: 10, 13: 50, 24: 40, 3: 30}
# a = str(test_dict.__sizeof__())
# print(a)

# solution2
# import sys
# a = str(sys.getsizeof(test_dict))
# print(a)

# ******************************************Ways to sort list of dictionaries by values in Python – Using itemgetter****
# keyword - itemgetter
# solution1
# from operator import itemgetter
# test_dict = [{ "Brand_id" : "GS", "Brand" : "GLS"},
# { "Brand_id" : "AB", "Brand" : "ABC"},
# { "Brand_id" : "MR", "Brand" : "MRE" },       #fully refered from w3 school
# { "Brand_id" : "NS", "Brand" : "NS" }]
# print(sorted(test_dict, key=itemgetter("Brand","Brand_id")))

# ************************Ways to sort list of dictionaries by values in Python – Using lambda function*****************
# solution1
# test_dict = [{ "Brand_id" : "GS", "Brand" : "GLS"},
# { "Brand_id" : "AB", "Brand" : "ABC"},
# { "Brand_id" : "MR", "Brand" : "MRE" },
# { "Brand_id" : "NS", "Brand" : "NS" }]
# a = sorted(test_dict,key = lambda x: x["Brand"])
# print(a)

# ******************Python | Merging two Dictionaries*******************************************************************
# solution1
# def mm(test_dict,test_dict1):
#     final_dict = {**test_dict, **test_dict1}
#     return final_dict
# test_dict = { "Brand_id" : "GS", "Brand" : "GLS"}
# test_dict1 = { "Brand_id" : "AB", "user_id" : "ABC"}     # if the key are same it will overwrite and return the last key value pair
# print(mm(test_dict,test_dict1))

# solution2
# def mm(test_dict,test_dict1):
#     final_dict = test_dict | test_dict1
#     return final_dict                     # this method was not working but i refered in w3 school website.
# test_dict = { "Brand_id" : "GS", "Brand" : "GLS"}
# test_dict1 = { "user" : "AB", "user_id" : "ABC"}
# print(mm(test_dict,test_dict1))

# *********************Program to create grade calculator in Python*****************************************************
# Requirements:
# 10 % of marks scored from submission of Assignments
# 70 % of marks scored from Test
# 20 % of marks scored in Lab-Works
# 1. score >= 90 : "A"
# 2. score >= 80 : "B"
# 3. score >= 70 : "C"
# 4. score >= 60 : "D"

# a = {"name": "Jack Frost",
#         "assignment": [80, 50, 40, 20],
#         "test": [75, 75],
#         "lab": [78.20, 77.20]
#         }
# b = {"name": "James Potter",
#          "assignment": [82, 56, 44, 30],
#          "test": [80, 80],
#          "lab": [67.90, 78.72]
#          }
# c = {"name": "Dylan Rhodes",
#          "assignment": [77, 82, 23, 39],
#          "test": [78, 77],
#          "lab": [80, 80]
#          }
# d = {"name": "Jessica Stone",
#         "assignment": [67, 55, 77, 21],
#         "test": [40, 50],
#         "lab": [69, 44.56]
#         }
# e = {"name": "Tom Hanks",
#        "assignment": [29, 89, 60, 56],
#        "test": [65, 56],
#        "lab": [50, 40.6]
#        }
# def avg(marks):
#     total = sum(marks)
#     total_sum = float(total)
#     total_sum1 = total_sum/len(marks)
#     return total_sum1
# def indiv_avg(students):
#     assignment = avg(students["assignment"])
#     test = avg(students["test"])
#     lab = avg(students["lab"])
#     return (0.1*assignment+0.7*test+0.2*lab)
#
# def grade(score):
#     if score >= 90:
#         return "A"
#     elif score >= 80:
#         return "B"
#     elif score >= 70:
#         return "C"
#     elif score >= 60:
#         return "D"
#     else:
#         return "E"
#
# def class_avg(students):
#     response = []
#     for students in students:
#         stu_avg = indiv_avg(students)
#         response.append(stu_avg)
#         return avg(response)
# students = [a,b,c,d,e]
#
# for i in students:
#     print(i["name"])
#     print(i["name"],"-", indiv_avg(i))
#     print(i["name"],"-", grade(indiv_avg(i)))
#
# class_avg = class_avg(students)
# print(class_avg)
# print(grade(class_avg))

# ************************************Python – Insertion at the beginning in OrderedDict********************************
# solution1
# from collections import OrderedDict
# test_dict = OrderedDict([{ "Brand_id" : "GS" , "Brand" : "GLS"}])
# # test_dict1 = { "user" : "AB"}
# update = test_dict.update({ "user" : "AB"})     #it will add the value at beginning but remaining values are not displayed only key displaying
# update1 = test_dict.move_to_end("user", last = False)
# print(test_dict)

# ********************************Python | Check order of character in string using OrderedDict( )**********************
# solution1
# from collections import OrderedDict
# def checking_string(input, string):
#     dict = OrderedDict.fromkeys(input)
#     str_length = 0
#     for key,value in dict.items():
#         if (key == string[str_length]):
#             str_length = str_length + 1
#         if (str_length == len(string)):
#             return 'true'
#     return 'false'
# if __name__ == "__main__":
#     input = "ajmal hussain"
#     string = "na"
#     print(checking_string(input,string))

# ***************************Dictionary and counter in Python to find winner of election********************************
# solution1
# from collections import Counter
# test_dict = Counter(['Ajmal','hussain','ajj','Ajmal','Ajmal','ajmal',
#     'hussain','hussain','ajj','ajj','Ajmal','hussain','Ajmal'])
# mvote = max(test_dict.values())
# # print("+++++++++++++++++++++++++++++++++",mvote)
# for i in test_dict.keys():
#     if test_dict[i] == mvote:
#         print(i)

# ***********************Python – Key with maximum unique values********************************************************
# solution1
# test_dict = {
#         "dbname": [1,4,5,6,7,8,1],
#         "tenant_dbname": [1,5,4,68,4,6],
#         "host": [4,9,0,4,55,8,5,3],
#         "rr_host": [1,2,3,4,5,6,7,7,3]
#       }
# dupli = []
# for i in test_dict.values():
#     var = i
#     for j in var:
#         new = i.count(j)            #it takes the lenth of unique values in dict
#         if new == 1:
#             dupli.append(i)
#     print(len(dupli))
#     dupli.clear()

# solution2
# dupli = 0
# mdupli = []
# for i in test_dict:
#     if len(set(test_dict[i])) > dupli:
#         # print(i)
#         dupli = len(set(test_dict[i]))
#         # print(dupli)
#         mdupli = i
# # mdupli.append(i)
# print(mdupli)

# solution3
# unique = sorted(test_dict,key = lambda i : len(set(test_dict[i])), reverse = True)[0]
# print(unique)

# *************************Python – Find all duplicate characters in string*********************************************
# solution1
# from collections import Counter
# test_dict = Counter({
#         "dbname": "ajmalhussain",
#         "tenant_dbname": "ajmal"
#       })
# dupli = 0                               # Not working
# for i in test_dict.values():
#     dupli = dupli + 1
#     print(dupli)
#     if i > 1:
#         print(test_dict.values()[dupli])

# solution2
# def dup(input):
#     test_dict1 = Counter(input)
#     # return test_dict1
#     dupli = -1                               # Not working
#     for i in test_dict1.values():
#         dupli = dupli + 1
#         # print(i)
#         if i > 1:
#             print test_dict1.keys()[dupli],
# if __name__ == "__main__":
#     input = "ajmalhussain"
#     print(dup(input))
# *****************************Python – Group Similar items to Dictionary Values List***********************************
# solution1
# from collections import Counter
# test_dict = [1,4,5,6,7,8,1,3,4,5,6,6,3,3,4,5,3,2,2,5,2,2,3,4,3,2]
# k = {}
# for key, value in Counter(test_dict).items():
    # b = key * value
    # print(b)
#     k[key] = [key] * value
# print(k)

# Counter(test_dict)
# print(Counter(test_dict))

# ************************************Python | Ways to remove a key from dictionary*************************************
# solution1
# test_dict = {
#         "dbname": "ajmalhussain",
#         "tenant_dbname": "ajmal"
#       }

# del test_dict["tenant_dbname"]
# print(test_dict)

# solution2
# rem = test_dict.pop('dbname')
# print(test_dict)

# solution3
# rem = test_dict.pop("dbname")
# print(test_dict)

# ********************************************Python – Remove Dictionary Key Words in the string************************
# solution
# string = "ajmal is the good boy"
# test_dict = {
#     "ajmal": 1,
#     "hussain":2
# }
# for i in test_dict:
#     print(i)
#     if i in string.split(" "):
#         print(i)
#         string = string.replace(i," ")
# print(string)

# ***************************Python | Remove all duplicates words from a given sentence*********************************
# solution1
# from collections import Counter
# string = "ajmal boy is an good boy"
# sp = string.split()
# num = []
# for i in sp:
#     if string.count(i) >= 1 and i not in num:
#         num.append(i)
# print(" ".join(num))

# **************************************Python – Remove duplicate values across Dictionary Values***********************
# solution1
# from collections import Counter
# def compare(test_dict):
#     k = []
#     for key, value in test_dict.items():
#         for values in value:
#             k.append(values)
#     a = Counter(k)
#     print(a)
#
# test_dict = {
#         "dbname": [1,2,3,4,5],
#         "tenant_dbname": [1,4,1]
#       }
# compare(test_dict)

# test_dict = {
#     "dbname": [1,2,3,5],
#     "tenant_dbname": [1,4]
#     }
# a=test_dict["dbname"]
# print(a)
# b=test_dict["tenant_dbname"]
# print(b)
# for x in a:
#     print(x)
#     for y in b:
#         print(y)
#         if x==y:
#             b.remove(y)
#         a.remove(x)
# #a.remove(x)
# print(test_dict)

# k = {}
# for key, value in Counter(test_dict).items():
#     b = key * value
#     print(b)
#     k[key] = [key] * value
# print(k)
# test_dict = {
#         "dbname": [1,2,3,4,5],
#         "tenant_dbname": [1,4,1]
#       }
# k = []
# for key, value in test_dict.items():
#     for values in value:
#         k.append(values)
# print(k)
# -------------------------------------2022-03-27-----------------------------------------------------------------------
# empty = Counter()
# for i in test_dict.values():
#     empty.update(i)
#     print(i)
# sol = {
#     i : [value for value in j if empty[value] == 1]
#         for i, j in test_dict.items()
# }
# print(sol)

# ***************************Python Dictionary to find mirror characters in a string************************************
# --------------------------------------------2022-03-28----------------------------------------------------------------
# def mirror(input, n):
#     string = 'abcdefghijklmnopqrstuvwxyz'
#     rstring = 'zyxwvutsrqponmlkjihgfedcba'
#     charac = dict(zip(string,rstring))
#     print(charac)
#
#     first = input[0:n-1]
#     last = input[n-1:]
#     mirror = ''
#     print("first:",first, "last:",last, "mirror:",mirror)
#
#     for i in range(0, len(last)):
#         mirror = mirror + charac[last[i]]
#         print(mirror)
#     print("final mirror:",mirror)
#     print (first + mirror)
#
# if __name__ == '__main__':
# input = 'kaarunnya'
# n = 6
# mirror(input,n)

# ***********************************Counting the frequencies in a list using dictionary in Python**********************
#  solution1
# from collections import Counter
# test_dict = [1,4,5,6,7,8,1,3,4,5,6,6,3,3,4,5,3,2,2,5,2,2,3,4,3,2]
# for i, j in Counter(test_dict).items():
#     print(i,":",j)

# solution2
# aj = {}
# for i in test_dict:
#     a= test_dict.count(i)
#     aj[i] = a
# print(aj)

# solution3
# aj = []
# for i in test_dict:
#     a= test_dict.count(i)    # testing for keyword "zip" -
#     aj.append(a)
# print(aj)
# charc = list(zip(test_dict,aj))
# print(charc)

# ***********************************************Python – Dictionary Values Mean****************************************
# solution1
# test_dict = {
#         "dbname": 1,
#         "tenant_dbname": 4,
#         "host":6,
#         "username":9,
#         "password":8
#       }
# a = []
# for value in (test_dict.values()):
#     a.append(value)
# if len(a) % 2 == 0:
#     b = round(len(a)/2)
#     c = a[b-1]
#     print(c)
# else:
#     b = round(len(a) / 2)
#     c = a[b]
#     print(c)

# test_dict = {'Gfg': 4, 'is': 7, 'Best': 8, 'for': 6, 'Geeks': 10}
# a = []
# for value in test_dict.values():
#     a.append(value)
# print(sum(a)/len(a))

# *****************Python counter and dictionary intersection example (Make a string using deletion and rearrangement)**
# solution1
# def check(input,s2):
#     for i in input:
#         if i in s2:
#             print(i, "possible")
#         else:
#             print(i, "not possible")
# if __name__ == '__main__':
#     s2 = 'abc'
#     input = 'kaarunnya'
#     check(input,s2)

# solution2
# from collections import Counter
# def check(s1,s2):
#     r1 = Counter(s1)
#     r2 = Counter(s2)
#     r3 = r1&r2
#     return r3 == r1
# if __name__ == '__main__':
#     s1 = 'abc'
#     s2 = 'kaarunnya'
#     if check(s1, s2)== True:
#         print("possible")
#     else:
#         print("not possible")

# ******************************Python dictionary, set and counter to check if frequencies can become same**************
# --------------------------------------------2022-03-29----------------------------------------------------------------
# from collections import Counter
# def cross(s1):
#     s2 = Counter(s1)
#     print(s2)
#     s3 = list(set(s2.values()))
#     print(s3)
#     if len(s3) > 2:
#         print("not possible")
#     elif len(s3) == 2 and s3[1] - s3[0]>1:
#         print("summa possible")
#     else:
#         print('possible')
# s1 = 'xyyzxxxz'
# cross(s1)

# **********************************Possible Words using given characters in Python*************************************
# def function1(word):
#     dict = {}
#     for i in word:
#         dict[i] = dict.get(i, 0) +1
#         print(dict)
#     print(dict)
#     return dict

# def function2(wordl, letter):
#     for word in wordl:
#         flag = 1
#         f1 = function1(word)
#         print(f1)
#         for a in f1:
#             print(a)
#             if a not in letter:
#                 flag = 0
#             else:
#                 if letter.count(a) != f1[a]:
#                     flag = 0
        # if flag == 1:
        #     print(word)
# if __name__ == '__main__':
#     word = ['go', 'bat', 'me', 'eat', 'goal', 'boy', 'run']
#     letter = ['e', 'o', 'b', 'a', 'm', 'g', 'l']
#     # function2(input, letter)
#     function1(word)
# **********************************Python – Maximum record value key in dictionary*************************************
# solution1
# test_dict = {'a' : {'d' : 11, 'e' : 20},
#              'b' : {'d' : 8, 'e' : 9},
#              'c' : {'d' : 10, 'e' : 15}}
# key = 'e'
#
# res = None
# res_max = 0
# for sub in test_dict:
#     # print(sub)
#     if test_dict[sub][key] > res_max:
#         res_max = test_dict[sub][key]
#         # print(res_max)
#         res = sub
# print(res)

# solution2
# def maxi(key):
#     r = None
#     a = 0
#     for i in test_dict:
#         if test_dict[i][key] > a:
#             a = test_dict[i][key]
#             r = i
#     print(r)
# key = 'd'
# maxi(key)

# **********************************Python – Extract values of Particular Key in Nested Values**************************
# def val(test_dict, key):
#     v = []
#     for i in test_dict:
#         a = test_dict[i][key]
#         v.append(a)
#     print(v)
# test_dict = {'Gfg': {'a': 7, 'b': 9, 'c': 12},
#                 'is': {'a': 15, 'b': 19, 'c': 20},
#                 'best':{'a': 5, 'b': 10, 'c': 2}
#     }
# key = 'c'
# val(test_dict,key)